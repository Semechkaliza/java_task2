import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class Element {
    private String name;
    private Element parent;
    private List<String> content = new LinkedList<>();
    private List<Element> childs = new LinkedList<>();
    private HashMap<String,String> attribute = new HashMap<>();

    Element(Element parentEl){
        this.parent=parentEl;
    }

    public void addAttribute(String key, String value){
        attribute.put(key,value);
    }

    public void addContent(String contentText){
        content.add(contentText);
    }

    public void setName(String nameEl){
        name=nameEl;
    }

    public void addChildren(Element element){
        childs.add(element);
    }

    public Element getParent() {
        return parent;
    }

    @Override
    public String toString() {
        return "Element{" +
                "name='" + name + '\'' +
                ", parent=" + parent +
                ", content=" + content +
                ", childs=" + childs +
                ", attribute=" + attribute +
                '}';
    }

}
