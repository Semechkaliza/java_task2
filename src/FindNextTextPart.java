public class FindNextTextPart {
    public static String findNextContent(String workText){
        String content = workText.substring(0,workText.indexOf('<'));
     //   System.out.println("Content:"+content);
        return content;
    }

    public static String findNextTextPart(String workText){
        String workPart = workText.substring(workText.indexOf('<')+1,workText.indexOf('>'));
     //   System.out.println("WorkPart:"+workPart);
        return workPart;
    }

    public static String findNextWorkText(String workText){
        workText = workText.substring(workText.indexOf('>')+1);
      //  System.out.println("WorkText:"+workText);
        return workText;
    }
}
