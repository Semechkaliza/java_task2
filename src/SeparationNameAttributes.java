public class SeparationNameAttributes {
    public static String getFirstWorkPart(String fileText){
       String workPart=fileText.substring(1,fileText.indexOf('>'));
      return workPart;
    }
    public static String getElementName(String workRart) {
        String name = workRart;
        if (workRart.contains(" ")) {
            name = name.substring(0, name.indexOf(' '));
        }
        return name;
    }
    public static void separateAttributes(String workPart,Element currentEl){
        while(!workPart.isEmpty()){
            if(workPart.indexOf(' ')==0) {
                workPart=workPart.substring(1);
            }
            String key =workPart.substring(0,workPart.indexOf('='));
            workPart=workPart.substring(workPart.indexOf('"')+1);
            String value=workPart.substring(workPart.indexOf('"'));
            workPart=workPart.substring(workPart.indexOf('"')+1);
            currentEl.addAttribute(key,value);
        }
    }
}
